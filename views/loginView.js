function login() {
  const { db } = require('../controllers/registerController');
  const { mainmenu } = require('./mainmenu');
  const { MenuCN } = require('./menuCN');
  var prompt = require('prompt');

  const properties = [
    {
      name: "username",
    },
    {
      name: "password",
      hidden: true,
    },

  ];


  prompt.start();
  console.log('Login');

  prompt.get(properties, function (err, result) {
    if (db.get("data").find({ username: result.username, password: result.password }).value() ) {
      MenuCN(result.username)
    } else {
       console.log('Enter wrong username, password or account not exist');
       mainmenu();
    }
  });
}
  module.exports.login = login;
