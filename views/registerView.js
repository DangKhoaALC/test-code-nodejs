const { db } = require('../controllers/registerController');
const { mainmenu } = require('./mainmenu');

function register(){
  var prompt = require('prompt');
    const mark ={
     properties: {
          username: {
            pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3,}$/,
            message: 'Username must be greater than 3 characters, at least a letter, a number and a special character',
            required: true
          },
          password: {
            pattern: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/,
            message: 'Password must be greater than 6 characters, at least a letter, a number and a special character',
            hidden: false
          },
          confirmpassword: {
            hidden: true
          }
        }
}  

    prompt.start();
    console.log('Register Account: ');
   
    prompt.get(mark, function (err, result) {
     if(result.confirmpassword != result.password)
     {
       console.log('Wrong password');
       mainmenu();
     }else{
       if(db.get('data').find({ username: result.username }).value())
       {
         console.log('User existed');
         mainmenu();
       }else{
         db.update('count',n=>n+1).write(),
        db.get('data')
        .push({id: db.get('count').value(), username:result.username,password:result.password})
        .write()
        console.log('Your Account:')
        console.log('  username: ' + result.username);
        console.log('  password: ' + result.password);
        console.log('Main Menu: ');
        mainmenu();
       }
     }
    });
};
module.exports.register = register;
    