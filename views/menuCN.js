function MenuCN(username) {
    const { AllReminder } = require("./allReminder");
    const { mainmenu } = require("./mainmenu");
    const { Reminder } = require("./registerReminder"); 
    const prompt = require('prompt');
    console.log('MenuCN: ')
    console.log('[0] Sign up')
    console.log('[1] Registered')
    console.log('[2] Back to main menu')

    prompt.get(['Case'], function (err, result) {
        var Case = result.Case;
        if (Case == 0) {
            Reminder(username);
        }

        if (Case == 1) {

            AllReminder(username);
        }
        if (Case == 2) {
            mainmenu();
        }
    })
}

module.exports.MenuCN = MenuCN;


