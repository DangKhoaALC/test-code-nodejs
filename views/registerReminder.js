const { dbreminder } = require('../controllers/remiderController');

function Reminder(username) {
    const { mainmenu } = require('./mainmenu');
    const { UserId } = require('../controllers/UserId');
    const prompt = require('prompt');
    const properties = [
        {
            name: 'day',
            validator: /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/,

        },
        {
            name: 'content',

        },
    ];
    console.log('Register Reminder: ');

    prompt.start();

    prompt.get(properties, async function (err, result) {
        await dbreminder.get('data').push({ id: UserId(username),day: result.day, content: result.content })
        .write();
        mainmenu();
    });

}
module.exports.Reminder = Reminder;
