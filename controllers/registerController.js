const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('data/data.json')
const db = low(adapter)
db.defaults({ count: 0, data: []}).write()
module.exports.db= db;
