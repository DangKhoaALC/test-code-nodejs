const { db } = require("./registerController");
function UserId(name) {
    var arr = db.get('data').value();
    var size = db.get('data').size().value();

    for (let i = 0; i < size; i++) {
        if (arr[i].username == name) {
            return arr[i].id;
        }
    }
}
module.exports.UserId = UserId;
