const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('data/dbReminder.json')
const dbreminder = low(adapter)
dbreminder.defaults({ data: []}).write()
module.exports.dbreminder= dbreminder;
